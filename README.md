# Eureka Example
Example of connecting to Eureka using `Fargo` library.

* `Fargo` logging is very noisy. To turn off, add `logging.SetLevel(logging.ERROR, "fargo")`

## Connecting
To connect to Eureka with `Fargo`, create a `fargo.Config` variable and 
populate with the Server's url, connection timeout, poll interval, and retries.

```go
var c fargo.Config
c.Eureka.ServiceUrls = []string{"http://localhost:8761/eureka"}
c.Eureka.ConnectTimeoutSeconds = 10
c.Eureka.PollIntervalSeconds = 30
c.Eureka.Retries = 3
eurekaConnection := fargo.NewConnFromConfig(c)
```

### Registering
To register the application with Eureka, create a `fargo.Instance` object.

```go
instance := &fargo.Instance{
    App:              "TESTAPP",
    HostName:         "127.0.0.1",
    IPAddr:           "127.0.0.1",
    VipAddress:       "127.0.0.1",
    SecureVipAddress: "127.0.0.1",
    Port:             8080,
    Status:           fargo.UP,
    UniqueID: func(i fargo.Instance) string {
        return fmt.Sprintf("%s:%s", "127.0.0.1", "1")
    },
    DataCenterInfo: fargo.DataCenterInfo{Name: fargo.MyOwn},
}
instance.SetMetadataString("version", "latest")
instance.SetMetadataString("instanceId", "1")
err := eurekaConnection.RegisterInstance(instance)
```

### Heartbeat
There is not clear documentation from the `Fargo` library on how to do heartbeats.
So at this time, I believe to send heartbeats is to create a goroutine that sends
`eurekaConnection.HeartBeatInstance(instance)` every some time interval.

## Client Lookup
For a client to lookup the application, once it has been registered and is sending heartbeats, is to first connect to
Eureka the same way the application did. Then do a lookup based on the app `eurekaConnection.GetApp("TESTAPP")`.

The returned application will list all the instances associated with the app. It is up to the client to figure out which one
to call.

## Gitlab Build Pipeline
Another point of this project was to test Gitlab's CI Pipeline. I created a simple `.gitlab-ci.yml` configuration.

I observed other projects checking in the vendor directory. I did not want this. So I used `vgo vendor` to pull the dependencies.

## Other Libraries
Using `Fargo` directly was challenging because of the lack of documentation and examples. I highly suggest using `go-micro`
with `go-plugins` to connect to Eureka.

* Note, I am unable to get `go-micro` to play nice with `vgo` for some reason (dependencies will not be copied to the vendor 
directory). So I would suggest using `dep` instead if using `go-micro`