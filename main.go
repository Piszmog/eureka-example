package main

import (
    "github.com/julienschmidt/httprouter"
    "log"
    "net/http"
    "github.com/hudl/fargo"
    "fmt"
    "time"
    "github.com/op/go-logging"
)

func main() {
    logging.SetLevel(logging.ERROR, "fargo")
    var c fargo.Config
    c.Eureka.ServiceUrls = []string{"http://localhost:8761/eureka"}
    c.Eureka.ConnectTimeoutSeconds = 10
    c.Eureka.PollIntervalSeconds = 30
    c.Eureka.Retries = 3
    eurekaConnection := fargo.NewConnFromConfig(c)
    instance := &fargo.Instance{
        App:              "TESTAPP",
        HostName:         "127.0.0.1",
        IPAddr:           "127.0.0.1",
        VipAddress:       "127.0.0.1",
        SecureVipAddress: "127.0.0.1",
        Port:             8080,
        Status:           fargo.UP,
        UniqueID: func(i fargo.Instance) string {
            return fmt.Sprintf("%s:%s", "127.0.0.1", "1")
        },
        DataCenterInfo: fargo.DataCenterInfo{Name: fargo.MyOwn},
    }
    instance.SetMetadataString("version", "latest")
    instance.SetMetadataString("instanceId", "1")
    err := eurekaConnection.RegisterInstance(instance)
    if err != nil {
        log.Fatal("Failed", err)
    }
    go heartbeat(eurekaConnection, instance)
    router := httprouter.New()
    router.GET("/greet/:name", greet)
    log.Fatal(http.ListenAndServe(":8080", router))
}

func heartbeat(eurekaConnection fargo.EurekaConnection, instance *fargo.Instance) {
    for {
        time.Sleep(time.Second * 30)
        err1 := eurekaConnection.HeartBeatInstance(instance)
        if err1 != nil {
            log.Fatal("Failed", err1)
        }
    }
}

func greet(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
    writer.Write([]byte("Hello " + params.ByName("name")))
}
