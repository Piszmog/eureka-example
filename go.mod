module github.com/Piszmog/eureka-example

require (
	github.com/franela/goblin v0.0.0-20180407132755-cd5d08fb4ede
	github.com/golang/protobuf v1.1.0
	github.com/hudl/fargo v1.2.0
	github.com/julienschmidt/httprouter v0.0.0-20180411154501-adbc77eec0d9
	github.com/onsi/ginkgo v1.5.0
	github.com/onsi/gomega v1.4.0
	golang.org/x/net v0.0.0-20180530234432-1e491301e022
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f
	golang.org/x/text v0.3.0
	gopkg.in/yaml.v2 v2.2.1
)
